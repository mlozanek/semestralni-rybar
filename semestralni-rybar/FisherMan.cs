﻿using SFML.Audio;
using SFML.Graphics;
using SFML.Window;


namespace semestralni_rybar;

public class FisherMan : GameLoop
{
    public const int DefaultWindowWidth = 720;
    public const int DefaultWindowHeight = 720;

    public const string WindowTitle = "F  I  S  H  E  R  M  A  N";

    private Player _player = new Player();
    
    public static int amountOfFish;
    public static int fishKilled;
    public static bool enterScreen = true;
    public static bool endScreen;

    public static bool playerCanMove = true;

    public FisherMan() : base(DefaultWindowWidth, DefaultWindowHeight, WindowTitle, Color.Black)
    {

    }

    public override void LoadContent()
    {
        Debugging.LoadContent();
        TextManager.LoadContent();
        SoundManager.LoadSounds();
        Quotes.LoadContent();
        FishPicture.LoadContent();
    }

    public override void Initialize()
    {
        Map.DrawWater();
        SoundManager.PlayTheme();
    }

    public override void Update(GameTime gameTime)
    {
        _player.Update(gameTime);
        FishGenerator.GenerateFish(gameTime);
        
        TextManager.EnterScreenShow();

        for (var index = FishGenerator.RenderingFisheeees.Count - 1; index >= 0; index--)
        {
            var fish = FishGenerator.RenderingFisheeees[index];

            if (CollisionHandeler.CollisionCheck(_player.BoxPlayer, fish.BoxFish))
            {
                FishGenerator.RenderingFisheeees.RemoveAt(index);
                
                TextManager.GetString();

                TextManager.DisplayText = true;
                FishPicture.displayFishPicture = true;
                
                SoundManager.PlayPickup();

                amountOfFish++;
            }
        }

        if (TextManager.DisplayText)
        {
            TextManager.TextDismiss();
            _player.playerMoving = false;                
        }
        
        else
            _player.playerMoving = true;
        
        if(Keyboard.IsKeyPressed(Keyboard.Key.Escape))
        {
            Window.Close();
        }
        
    }

    public override void Draw(GameTime gameTime)
    {
        Animator.AnimateWaterTextures(this);
        
        foreach (var fish in FishGenerator.RenderingFisheeees)
        {
            fish.Render(this);
        }

        if (endScreen)
            EndingScreen.Render(this);
        
        _player.Render(this);

        FishPicture.Render(this);


        Debugging.RenderDebug(this, Color.Magenta);
        
        TextManager.RenderCollisionText(this, Color.Magenta);
        
        TextManager.OpeningText(this);
    }

}