﻿using SFML.Graphics;
using SFML.System;

namespace semestralni_rybar;

public class EndingScreen
{
    public static Text line1;
    public static Text line2;
    public static Text line3;

    public static int PosX = 40;
    public static int PosY = FisherMan.DefaultWindowHeight/4;
    
    public static void Render(GameLoop gameLoop)
    {
        RectangleShape background = new RectangleShape(new Vector2f(FisherMan.DefaultWindowWidth,FisherMan.DefaultWindowHeight));
        background.Position = new Vector2f(0,0);
        background.FillColor = Color.Black;
        background.OutlineColor = Color.Magenta;
        background.OutlineThickness = 5;
        
        Text text = new Text("ok that's the end", TextManager.ConsoleFont, TextManager.CharacterSize);
        text.Position = new Vector2f(PosX, PosY);
        text.FillColor = Color.Magenta;
        
        if(FisherMan.fishKilled<1)
            Ending1();
        if(FisherMan.fishKilled is >= 1 and <= 5)
            Ending2();
        if(FisherMan.fishKilled is > 5 and <= 9)
            Ending3();
        if(FisherMan.fishKilled>9)
            Ending4();
        
        gameLoop.Window.Draw(background);
        
        gameLoop.Window.Draw(text);
        
        gameLoop.Window.Draw(line1);
        gameLoop.Window.Draw(line2);
        gameLoop.Window.Draw(line3);
    }

    public static void Ending1()
    {
        line1 = new Text("so... you let all of the angry fish go", TextManager.ConsoleFont, TextManager.CharacterSize);
        line2 = new Text("and managed to keep your inner peace", TextManager.ConsoleFont, TextManager.CharacterSize);
        line3 = new Text("or you agree with them.. It's hard to tell", TextManager.ConsoleFont, TextManager.CharacterSize);

        line1.Position = new Vector2f(PosX, PosY + 40);
        line1.FillColor = Color.Magenta;
        
        line2.Position = new Vector2f(PosX, PosY + 80);
        line2.FillColor = Color.Magenta;
        
        line3.Position = new Vector2f(PosX, PosY + 120);
        line3.FillColor = Color.Magenta;
    }
    
    public static void Ending2()
    {
        line1 = new Text($"mmm... so you killed {FisherMan.fishKilled} of them", TextManager.ConsoleFont, TextManager.CharacterSize);
        line2 = new Text("but you also let some of them live", TextManager.ConsoleFont, TextManager.CharacterSize);
        line3 = new Text("so... how does that make you feel?", TextManager.ConsoleFont, TextManager.CharacterSize);

        line1.Position = new Vector2f(PosX, PosY + 40);
        line1.FillColor = Color.Magenta;
        
        line2.Position = new Vector2f(PosX, PosY + 80);
        line2.FillColor = Color.Magenta;
        
        line3.Position = new Vector2f(PosX, PosY + 120);
        line3.FillColor = Color.Magenta;
    }
    
    public static void Ending3()
    {
        line1 = new Text($"interesting. you killed {FisherMan.fishKilled} of them", TextManager.ConsoleFont, TextManager.CharacterSize);
        line2 = new Text("that's a lot of dead fish", TextManager.ConsoleFont, TextManager.CharacterSize);
        line3 = new Text("maybe... you yourself are a bit of an \n angry fish", TextManager.ConsoleFont, TextManager.CharacterSize);
        
        line1.Position = new Vector2f(PosX, PosY + 40);
        line1.FillColor = Color.Magenta;
        
        line2.Position = new Vector2f(PosX, PosY + 80);
        line2.FillColor = Color.Magenta;
        
        line3.Position = new Vector2f(PosX, PosY + 120);
        line3.FillColor = Color.Magenta;
    }
    
    public static void Ending4()
    {
        line1 = new Text("wow... you killed all of them", TextManager.ConsoleFont, TextManager.CharacterSize);
        line2 = new Text("and managed to keep your inner peace", TextManager.ConsoleFont, TextManager.CharacterSize);
        line3 = new Text("maybe you are an angry fish yourself ", TextManager.ConsoleFont, TextManager.CharacterSize);

        line1.Position = new Vector2f(PosX, PosY + 40);
        line1.FillColor = Color.Magenta;
        
        line2.Position = new Vector2f(PosX, PosY + 80);
        line2.FillColor = Color.Magenta;
        
        line3.Position = new Vector2f(PosX, PosY + 120);
        line3.FillColor = Color.Magenta;
    }
}