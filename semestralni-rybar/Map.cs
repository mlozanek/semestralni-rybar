﻿using System.Runtime.InteropServices;
using Microsoft.VisualBasic;
using SFML.Graphics;

namespace semestralni_rybar;

public class Map
{
    public static List<Tile> tiles = new List<Tile>();
    public static List<Tile> tiles2 = new List<Tile>();
    public static List<Tile> tiles3 = new List<Tile>();
    public static List<Tile> tiles4 = new List<Tile>();
    public static List<Tile> tiles5 = new List<Tile>();
    
    public const int MapWidth = (FisherMan.DefaultWindowWidth / 16)-1;
    public const int MapHeight = (FisherMan.DefaultWindowHeight / 16)-1;
    public const int BeachWidth = 1;

    public const int PierPosition = 20;
    
    private static int x = 0;
    private static int y = 0;

    public Map()
    {
        DrawWater();
        DrawBeach();
    }

    public static void DrawWater()
    {
        tiles.Clear();
        
        //Water
        while (y <= MapHeight)
        {
            x = 0;
            while (x <= MapWidth)
            {
                tiles.Add(new Tile(x, y, TextureManager.GenerateMapTileType()));
                tiles2.Add(new Tile(x, y, TextureManager.GenerateMapTileType()));
                tiles3.Add(new Tile(x, y, TextureManager.GenerateMapTileType()));
                tiles4.Add(new Tile(x, y, TextureManager.GenerateMapTileType()));
                tiles5.Add(new Tile(x, y, TextureManager.GenerateMapTileType()));
                
                //Buoy

                if (y == 0 && (x % 2 == 0))
                {
                    tiles.Add(new Tile(x,y,TextureManager.Buoy));
                    tiles2.Add(new Tile(x,y,TextureManager.Buoy));
                    tiles3.Add(new Tile(x,y,TextureManager.Buoy));
                    tiles4.Add(new Tile(x,y,TextureManager.Buoy));
                    tiles5.Add(new Tile(x,y,TextureManager.Buoy));
                }
                    
                if (y == MapHeight && (x%2==0))
                {
                    tiles.Add(new Tile(x,y,TextureManager.Buoy));
                    tiles2.Add(new Tile(x,y,TextureManager.Buoy));
                    tiles3.Add(new Tile(x,y,TextureManager.Buoy));
                    tiles4.Add(new Tile(x,y,TextureManager.Buoy));
                    tiles5.Add(new Tile(x,y,TextureManager.Buoy));
                }
                
                if(x== MapWidth && y%2==0)
                {
                    tiles.Add(new Tile(x,y,TextureManager.Buoy));
                    tiles2.Add(new Tile(x,y,TextureManager.Buoy));
                    tiles3.Add(new Tile(x,y,TextureManager.Buoy));
                    tiles4.Add(new Tile(x,y,TextureManager.Buoy));
                    tiles5.Add(new Tile(x,y,TextureManager.Buoy));
                }
                
                x++;
            }
            y++;
        }
       
        
        //Beach
        y = 0;
        while (y <= MapHeight)
        {
            x = 0;
            while (x <= BeachWidth)
            {
                tiles.Add(new Tile(x, y, TextureManager.Beach));
                tiles2.Add(new Tile(x, y, TextureManager.Beach));
                tiles3.Add(new Tile(x, y, TextureManager.Beach));
                tiles4.Add(new Tile(x, y, TextureManager.Beach));
                tiles5.Add(new Tile(x, y, TextureManager.Beach));
                x++;
            }
            tiles.Add(new Tile(x-1,y, TextureManager.BeachEdge));
            tiles2.Add(new Tile(x-1,y, TextureManager.BeachEdge));
            tiles3.Add(new Tile(x-1,y, TextureManager.BeachEdge));
            tiles4.Add(new Tile(x-1,y, TextureManager.BeachEdge));
            tiles5.Add(new Tile(x-1,y, TextureManager.BeachEdge));
            
            y++;
        }
        
        //Pier
        x = 0;
        while (x <= 4)
        {
            tiles.Add(new Tile(x, PierPosition, TextureManager.Pier));
            tiles2.Add(new Tile(x, PierPosition, TextureManager.Pier));
            tiles3.Add(new Tile(x, PierPosition, TextureManager.Pier));
            tiles4.Add(new Tile(x, PierPosition, TextureManager.Pier));
            tiles5.Add(new Tile(x, PierPosition, TextureManager.Pier));
            
            x++;
        }
        
        
        x = 0;
        y = 0;

    }

    public void DrawBeach()
    {
       
    }


    public class Tile
    {
        Sprite sprite;

        public int x;
        public int y;

        public bool hasPoi { get; set; }

        public Tile(int posX, int posY, Texture texture)
        {
            hasPoi = false;
            x = posX;
            y = posY;
            sprite = new Sprite(texture);
            sprite.Position = new SFML.System.Vector2f(x * TextureManager.TileSize, y * TextureManager.TileSize);
        }

        public void ReplaceTexture(Texture texture)
        {
            sprite.Texture = texture;
        }

        public void Render(GameLoop gameLoop)
        {
            gameLoop.Window.Draw(sprite);
        }
    }
}