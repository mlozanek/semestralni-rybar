﻿using SFML.Graphics;


namespace semestralni_rybar;

public class TextureManager
{
    public static int TileSize = 16;
    
    
    static string tilemapPath = "assets/tilemap.png";

    private static Random _random = new Random();

    public static int positionX = 1;
    public static int postiionY = 0;

    static Texture LoadTextureFromTileMap(int posX, int posY)
    {
        IntRect area = new IntRect(posX * TileSize, posY * TileSize, TileSize, TileSize);
        return new Texture(tilemapPath, area);
    }

    public static Texture GenerateMapTileType()
    {
        positionX = _random.Next(2, 4);
        postiionY = _random.Next(0, 2);
        
        WaterSurface = LoadTextureFromTileMap(positionX,postiionY);
        return WaterSurface;
    }

        public static Texture WaterSurface;
        
        public static Texture Player = LoadTextureFromTileMap(0, 0);
        
        public static Texture FishBlob = LoadTextureFromTileMap(2,2);

        public static Texture Buoy = LoadTextureFromTileMap(1, 0);
        public static Texture Pier = LoadTextureFromTileMap(3, 2);


        //Beach
        public static Texture Beach = LoadTextureFromTileMap(0, 3);
        public static Texture BeachEdge = LoadTextureFromTileMap(0, 2);

        //Fisheees
        public static Texture Fish1 = LoadTextureFromTileMap(0, 1);
        public static Texture Fish2 = LoadTextureFromTileMap(1, 1);
        public static Texture Fish3 = LoadTextureFromTileMap(1, 2);
        public static Texture Fish4 = LoadTextureFromTileMap(1, 3);
        public static Texture Fish5 = LoadTextureFromTileMap(2, 3);
        public static Texture Fish6 = LoadTextureFromTileMap(3, 3);
}