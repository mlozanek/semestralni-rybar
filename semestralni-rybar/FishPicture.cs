﻿using SFML.Graphics;
using SFML.System;

namespace semestralni_rybar;

public class FishPicture
{
    public static bool displayFishPicture;

    public static bool renderNextFish = true;

    private static Sprite _fishToBeRendered;
    
    private static Sprite _sprite1;
    private static Sprite _sprite2;
    private static Sprite _sprite3;
    private static Sprite _sprite4;
    private static Sprite _sprite5;
    private static Sprite _sprite6;

    private static Vector2f positionOfFishPicture;

    public static void LoadContent()
    {
        _sprite1 = new Sprite(TextureManager.Fish1);
        _sprite2 = new Sprite(TextureManager.Fish2);
        _sprite3 = new Sprite(TextureManager.Fish3);
        _sprite4 = new Sprite(TextureManager.Fish4);
        _sprite5 = new Sprite(TextureManager.Fish5);
        _sprite6 = new Sprite(TextureManager.Fish6);

        positionOfFishPicture = new Vector2f(130, 160);
        _fishToBeRendered = _sprite1;
    }

    public static void DisplayFish()
    {
        int previousRandomNumber=0;
        int randomNumber=0;
        
        if (renderNextFish)
        {
            int i = 0;
            
            while (randomNumber == previousRandomNumber && i<30)
            {
                randomNumber = new Random().Next(0, 6);
                i++;
            }

            switch (randomNumber)
            {
                case 1:
                    _fishToBeRendered = _sprite1;
                    break;
                case 2:
                    _fishToBeRendered = _sprite2;
                    break;
                case 3:
                    _fishToBeRendered = _sprite3;
                    break;
                case 4:
                    _fishToBeRendered = _sprite4;
                    break;
                case 5:
                    _fishToBeRendered = _sprite5;
                    break;
                case 6:
                    _fishToBeRendered = _sprite6;
                    break;
            }

            i = 0;
            renderNextFish = false;
        }
    }
    

    public static void Render(GameLoop gameLoop)
    {
        DisplayFish();
        
        if (displayFishPicture)
        {
            _fishToBeRendered.Scale = new Vector2f(20, 20);
            _fishToBeRendered.Position = positionOfFishPicture;
            Console.WriteLine(_fishToBeRendered.Position);
            gameLoop.Window.Draw(_fishToBeRendered);   
        }
    }
}