﻿using SFML.Graphics;
using SFML.System;

namespace semestralni_rybar;

public static class Animator
{
    public static int animationSpeed = 1;
    static Clock clock = new Clock();

    private static int x;

    public static void AnimateWaterTextures(GameLoop gameLoop)
    {
        if (x == 0)
        {
            foreach (Map.Tile t in Map.tiles)
            {
                t.Render(gameLoop);
            }
        }

        if (x == 1)
        {
            foreach (Map.Tile t in Map.tiles2)
            {
                t.Render(gameLoop);
            }
        }
        
        if (x == 2)
        {
            foreach (Map.Tile t in Map.tiles3)
            {
                t.Render(gameLoop);
            }
        }
        
        if (x == 3)
        {
            foreach (Map.Tile t in Map.tiles4)
            {
                t.Render(gameLoop);
            }
        }
        
        if (x == 4)
        {
            foreach (Map.Tile t in Map.tiles5)
            {
                t.Render(gameLoop);
            }
        }
        
        /*
        switch (x)
        {
            case 0:
                foreach (Map.Tile t in Map.tiles)
                {
                    t.Render(gameLoop);
                }
                break;
            case 1:
                foreach (Map.Tile t in Map.tiles2)
                {
                    t.Render(gameLoop);
                }
                break;
            case 2:
                foreach (Map.Tile t in Map.tiles3)
                {
                    t.Render(gameLoop);
                }
                break;
            case 3:
                foreach (Map.Tile t in Map.tiles4)
                {
                    t.Render(gameLoop);
                }
                break;
            case 4:
                foreach (Map.Tile t in Map.tiles5)
                {
                    t.Render(gameLoop);
                }
                break;
        }
        */

        if (clock.ElapsedTime.AsSeconds() > animationSpeed)
        {
            // if (x < 4)
            //     x++;
            // else
            //     x = 0;
            //
            
            
            clock.Restart();        
        }
    }
}