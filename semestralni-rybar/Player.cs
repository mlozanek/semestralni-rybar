﻿
using SFML.Graphics;
using SFML.Window;

namespace semestralni_rybar;

public class Player
{
    public string Name;

    public float PosX = 30;
    public float PosY = 400;

    public float MoveSpeed;
    public float MoveSpeedConstant = 100f;

    public bool playerMoving = true;
    
    public FloatRect BoxPlayer = new FloatRect();

    private bool isInEncounter = false;

    private CollisionHandeler _collider = new CollisionHandeler();
    public Sprite Sprite;

    public Player ()
    {
        Sprite = new Sprite(TextureManager.Player);
        
        BoxPlayer.Height = _collider.BoxSize;
        BoxPlayer.Width = _collider.BoxSize;
    }

    public void ColliderUpdate()
    {
        BoxPlayer.Left = PosX;
        BoxPlayer.Top = PosY;
    }
    
    public void MoveLeft ()
    {
        float newPosX = PosX - MoveSpeed;

        if (newPosX >= (Map.BeachWidth*TextureManager.TileSize))
        {
            PosX = newPosX;
        }
    }

    public void MoveRight ()
    {
        float newPosX = PosX + MoveSpeed;

        if (newPosX >= 0 && newPosX < FisherMan.DefaultWindowWidth-TextureManager.TileSize)
        {
            PosX = newPosX;
        }
    }

    public void MoveUp ()
    {
        float newPosY = PosY - MoveSpeed;

        if (newPosY >= 0)
        {
            PosY = newPosY;
        }
    }

    public void MoveDown ()
    {
        float newPosY = PosY + MoveSpeed ;

        if (newPosY >= 0 && newPosY < FisherMan.DefaultWindowHeight-TextureManager.TileSize)
        {
            PosY = newPosY;
        }
    }
    
    public void Update (GameTime gameTime)
    {
        ColliderUpdate();
        
        MoveSpeed = MoveSpeedConstant*gameTime.DeltaTime;

        if (playerMoving)
        {
            if (Keyboard.IsKeyPressed(Keyboard.Key.W))
            {
                MoveUp();
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.S))
            {
                MoveDown();
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.A))
            {
                MoveLeft();
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.D))
            {
                MoveRight();
            }   
        }
    }
    
    public void Render (GameLoop gameLoop)
    {
        Sprite.Position = new SFML.System.Vector2f(PosX, PosY);
        
        gameLoop.Window.Draw(Sprite);
    }
}

