﻿using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using SFML.Graphics;
using SFML.System;

namespace semestralni_rybar;

public class Fish
{
    public string Name;

    public int Id;
    public float PosX;
    public float PosY;
    public Sprite _sprite;
    
    public FloatRect BoxFish = new FloatRect();
    
    
    private Random _random = new Random();
    private CollisionHandeler _collider = new CollisionHandeler();
   
    public Fish( int id, float x, float y, Sprite sprite)
    {
        Id = id;   
        PosX = x;
        PosY = y;
        _sprite = sprite;
        _sprite.Position = new Vector2f(PosX, PosY);
        
        BoxFish.Height = _collider.BoxSize;
        BoxFish.Width = _collider.BoxSize;
        BoxFish.Left = PosX;
        BoxFish.Top = PosY;
    }

    public void Render(GameLoop gameLoop)
    {
        gameLoop.Window.Draw(_sprite);
    }

} 