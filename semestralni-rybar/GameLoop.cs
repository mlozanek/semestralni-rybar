﻿using SFML.Graphics;
using SFML.Window;
using SFML.System;

namespace semestralni_rybar;

public abstract class GameLoop
{
    public const int TargetFps = 60;
    public const float TimeUnitUpdate = 1f / TargetFps;

    public RenderWindow Window { get; set; }

    public GameTime GameTime { get; set; }

    public Color WindowClearColor { get; set; }
    
    public GameLoop(uint windowWidth, uint windowHeight, string windowTitle, Color windowClearColor)
    {
        this.WindowClearColor = windowClearColor;
        this.Window = new RenderWindow(new VideoMode(windowWidth, windowHeight), windowTitle);
        this.GameTime = new GameTime();

        Window.Closed += Window_Closed;
    }

    public void Run()
    {
        LoadContent();
        Initialize();

        float totalTimeBeforeUpdate = 0f;
        float previousTimeElapsed = 0f;
        float deltaTime = 0f;
        float totalTimeElapsed = 0f;

        Clock clock = new Clock();

        while (Window.IsOpen)
        {
            Window.DispatchEvents();

            totalTimeElapsed = clock.ElapsedTime.AsSeconds();
            deltaTime = totalTimeElapsed - previousTimeElapsed;
            previousTimeElapsed = totalTimeElapsed;

            totalTimeBeforeUpdate += deltaTime;

            if (totalTimeBeforeUpdate >= TimeUnitUpdate)
            {
                GameTime.Update(totalTimeBeforeUpdate, totalTimeElapsed);
                totalTimeBeforeUpdate = 0f;
                
                Update(GameTime);
                
                Window.Clear(WindowClearColor);
                
                Draw(GameTime);
                Window.Display();
                
            }
        }
    }
    
    public abstract void LoadContent();
    public abstract void Initialize();
    public abstract void Update(GameTime gameTime);
    public abstract void Draw(GameTime gameTime);

    void Window_Closed(object sender, EventArgs eventArgs)
    {
        Window.Close();
    }
}