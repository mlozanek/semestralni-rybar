﻿using System.Runtime.CompilerServices;
using SFML.Graphics;

namespace semestralni_rybar;

public static class FishGenerator
{
    public static int posX;
    public static int posY;
    
    public static int MaxNumberOfFish = 1;
    public static int numberOfFish = 0;

    public static List<Fish> RenderingFisheeees = new List<Fish>(20);
    
    private static Random _random = new Random();
    static Sprite sprite;
    private static int _fishId = 1;

    public static float GeneratePositionX()
    {
        posX = _random.Next(30, FisherMan.DefaultWindowWidth-TextureManager.TileSize);
        return posX;
    }

    public static float GeneratePositionY()
    {
        posY = _random.Next(0, FisherMan.DefaultWindowHeight-TextureManager.TileSize);
        return posY;
    }

 
     public static void GenerateFish(GameTime gameTime)
     {
         if (gameTime.TotalTimeElapsed > 3 && gameTime.TotalTimeElapsed < 10)
         {
             MaxNumberOfFish = 2;
         }
         else if (gameTime.TotalTimeElapsed >10 && gameTime.TotalTimeElapsed < 20)
         {
             MaxNumberOfFish = 3;
         }
         else if (gameTime.TotalTimeElapsed >20 && gameTime.TotalTimeElapsed < 30)
         {
             MaxNumberOfFish = 6;
         }
         else if (gameTime.TotalTimeElapsed >30)
         {
             MaxNumberOfFish = 10;
         }
         
         if (numberOfFish < MaxNumberOfFish)
         {
             RenderingFisheeees.Add(new Fish(_fishId, GeneratePositionX(), GeneratePositionY(), new Sprite(TextureManager.FishBlob)));
             _fishId++;
             numberOfFish++;
         }
     }
}
