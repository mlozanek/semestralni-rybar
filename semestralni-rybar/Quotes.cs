﻿namespace semestralni_rybar;

public class Quotes
{
    public static List<string> quotes;
    
    public static void LoadContent()
    {
        quotes = AllQuotes();
    }
    
    public static List<string> AllQuotes()
    {
        string[] input =
        {
            "i love jeff bezos",
            "earth is flat",
            "russia is the goodguy",
            "there are only 2 genders",
            "i drive my suv to the city centre",
            "recycling is for loosers",
            "climate change is a myth",
            "abortions should be illegal",
            "vaccines cause autism, did you know?",
            "minimum wage is high enough",
            "women belong to the kitchen"
        };

        List<string> quotes = new List<string>(input);
        return quotes;
    }
}