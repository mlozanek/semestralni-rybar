﻿using SFML.Audio;
using SFML.Graphics;
using SFML.System;

namespace semestralni_rybar;

public class SoundManager
{
    public static Music theme;
    public static SoundBuffer BufferPick;
    public static SoundBuffer BufferLetgo;
    public static SoundBuffer BufferKill;
    
    private static string themeMusicPath = "assets/sound/themeF.wav";
    private static string pickupSoundPath = "assets/sound/pickup.wav";
    private static string killSoundPath = "assets/sound/kill.wav";
    private static string letgoSoundPath = "assets/sound/letgo.wav";
    

    public static void LoadSounds()
    {
        BufferPick = new SoundBuffer(pickupSoundPath);
        BufferLetgo = new SoundBuffer(letgoSoundPath);
        BufferKill = new SoundBuffer(killSoundPath);
    }
    
    public static void PlayTheme()
    {
        theme = new Music(themeMusicPath);
        theme.Loop = true;
        theme.Play();
        
    }
    
    public static void PlayPickup()
    {
        Sound sound = new Sound(BufferPick);
        sound.Play();
    }
    
    public static void PlayKill()
    {
        Sound sound = new Sound(BufferKill);
        sound.Play();
    }
    public static void PlayLetgo()
    {
        Sound sound = new Sound(BufferLetgo);
        sound.Play();
    }
}
