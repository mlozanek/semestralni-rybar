﻿using System.Drawing;
using SFML.Graphics;
using SFML.System;

namespace semestralni_rybar;

public class CollisionHandeler
{
    public int BoxSize = TextureManager.TileSize;
    

    public static bool CollisionCheck(FloatRect player, FloatRect fish)
    {
        return player.Intersects(fish);
    }
}