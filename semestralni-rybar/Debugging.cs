﻿using SFML.System;
using SFML.Graphics;
namespace semestralni_rybar;

public class Debugging
{
    public const string FontPath = "./assets/SpaceMono-Bold.ttf";

    public static Font? consoleFont;

    public static void LoadContent()
    {
        consoleFont = new Font(FontPath);
    }

    public static void RenderDebug(GameLoop gameLoop, Color color)
    {
        string totalTimeElapsedString = gameLoop.GameTime.TotalTimeElapsed.ToString("0.00");
        

        Text text = new Text(totalTimeElapsedString, consoleFont, 16);
        text.Position = new Vector2f(590, 4);
        text.FillColor = color;
        
        

        string deltaTimeString = gameLoop.GameTime.DeltaTime.ToString("0.0000");
        float fps = 1f / gameLoop.GameTime.DeltaTime;
        string fpsString = fps.ToString("0.0");
        
        
        Text text2 = new Text(deltaTimeString, consoleFont, 16);
        text2.Position = new Vector2f(110, 4);
        text2.FillColor = color;
        
        Text text3 = new Text(fpsString, consoleFont, 16);
        text3.Position = new Vector2f(210, 4);
        text3.FillColor = color;

        // gameLoop.Window.Draw(text);
        // gameLoop.Window.Draw(text2);
        // gameLoop.Window.Draw(text3);
 
    }
}