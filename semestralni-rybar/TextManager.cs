﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace semestralni_rybar;

public class TextManager
{
    public const string FontPath = "./assets/SpaceMono-Bold.ttf";

    public static Font? ConsoleFont;

    public static int PosX = FisherMan.DefaultWindowWidth/5;
    public static int PosY = FisherMan.DefaultWindowHeight/14;
    
    public static uint CharacterSize = FisherMan.DefaultWindowHeight / 30;

    public static bool DisplayText;
    public static string s;

    public static void LoadContent()
    {
        ConsoleFont = new Font(FontPath);
    }

    public static void OpeningText(GameLoop gameLoop)
    {
        if (FisherMan.enterScreen)
        {
            FisherMan.playerCanMove = false;
            
            RectangleShape background = new RectangleShape(new Vector2f(FisherMan.DefaultWindowWidth,FisherMan.DefaultWindowHeight));
            background.Position = new Vector2f(0,0);
            background.FillColor = Color.Black;

            Text cover = (new Text("welcome to", ConsoleFont, CharacterSize));
            cover.Position = new Vector2f(30,(FisherMan.DefaultWindowHeight/2)-150);

            Text cover2 = (new Text("F I S H E R M A N", ConsoleFont, 40));
            cover2.Position = new Vector2f(30, (FisherMan.DefaultWindowHeight/2)-60);
            cover2.FillColor = Color.White;
            
            Text instructions = (new Text("use WASD to move", ConsoleFont, 20));
            instructions.Position = new Vector2f(30, (FisherMan.DefaultWindowHeight/2)+90);
            
            Text instructions2 = (new Text("press /esc/ to leave", ConsoleFont, 20));
            instructions2.Position = new Vector2f(30, (FisherMan.DefaultWindowHeight/2)+120);
            
            Text instructions3 = (new Text("press /space/ to continue", ConsoleFont, 20));
            instructions3.Position = new Vector2f(30, (FisherMan.DefaultWindowHeight/2)+180);

            gameLoop.Window.Draw(background);
            gameLoop.Window.Draw(cover);
            gameLoop.Window.Draw(cover2);
            gameLoop.Window.Draw(instructions);
            gameLoop.Window.Draw(instructions2);
            gameLoop.Window.Draw(instructions3);
        }

        FisherMan.playerCanMove = true;
    }

    public static void EnterScreenShow()
    {
        if (Keyboard.IsKeyPressed(Keyboard.Key.Space))
        {
            FisherMan.enterScreen = false;
        }
    }


    public static void RenderCollisionText(GameLoop gameLoop, Color color)
    {
        if (DisplayText)
        {
            //h1
            Text h1 = new Text("you caught a fish", ConsoleFont, CharacterSize);
            h1.Position = new Vector2f(PosX, PosY);
            h1.FillColor = color;

            FloatRect h1Rect = h1.GetGlobalBounds();

            RectangleShape rect1 = new RectangleShape(new Vector2f(h1Rect.Width, h1Rect.Height));
            rect1.Position = new Vector2f(h1Rect.Left, h1Rect.Top);
            rect1.FillColor = Color.White;
            rect1.OutlineColor = Color.Blue;
            rect1.OutlineThickness = 3;
            
            //h2
            Text h2 = new Text("it looks at you and tells you", ConsoleFont, CharacterSize);
            h2.Position = new Vector2f(PosX, PosY+(2*h1Rect.Height));
            h2.FillColor = Color.Blue;

            FloatRect h2Rect = h2.GetGlobalBounds();
            
            RectangleShape rect2 = new RectangleShape(new Vector2f(h2Rect.Width, h2Rect.Height));
            rect2.Position = new Vector2f(h2Rect.Left, h2Rect.Top);
            rect2.FillColor = Color.White;
            rect2.OutlineColor = Color.Magenta;
            rect2.OutlineThickness = 3;
            
            //h3
            Text h3 = new Text(s, ConsoleFont, CharacterSize);
            h3.Position = new Vector2f(PosX, PosY+(4*h1Rect.Height));
            h3.FillColor = Color.Blue;

            FloatRect h3Rect = h3.GetGlobalBounds();
            
            RectangleShape rect3 = new RectangleShape(new Vector2f(h3Rect.Width, h3Rect.Height));
            rect3.Position = new Vector2f(h3Rect.Left, h3Rect.Top);
            rect3.FillColor = Color.White;
            rect3.OutlineColor = Color.Magenta;
            rect3.OutlineThickness = 3;
            
            //h4
            Text h4 = new Text("press /Q/ to kill the fish", ConsoleFont, CharacterSize);
            h4.Position = new Vector2f(PosX, PosY+(20*h1Rect.Height));
            h4.FillColor = Color.Magenta;

            FloatRect h4Rect = h4.GetGlobalBounds();
            
            RectangleShape rect4 = new RectangleShape(new Vector2f(h4Rect.Width, h4Rect.Height));
            rect4.Position = new Vector2f(h4Rect.Left, h4Rect.Top);
            rect4.FillColor = Color.White;
            rect4.OutlineColor = Color.Blue;
            rect4.OutlineThickness = 3;
            
            //h5
            Text h5 = new Text("press /E/ to leave it be", ConsoleFont, CharacterSize);
            h5.Position = new Vector2f(PosX, PosY+(22*h1Rect.Height));
            h5.FillColor = Color.Magenta;

            FloatRect h5Rect = h5.GetGlobalBounds();
            
            RectangleShape rect5 = new RectangleShape(new Vector2f(h5Rect.Width, h5Rect.Height));
            rect5.Position = new Vector2f(h5Rect.Left, h5Rect.Top);
            rect5.FillColor = Color.White;
            rect5.OutlineColor = Color.Blue;
            rect5.OutlineThickness = 3;

            gameLoop.Window.Draw(rect1);
            gameLoop.Window.Draw(h1);
            gameLoop.Window.Draw(rect2);
            gameLoop.Window.Draw(h2);
            gameLoop.Window.Draw(rect3);
            gameLoop.Window.Draw(h3);
            gameLoop.Window.Draw(rect4);
            gameLoop.Window.Draw(h4);
            gameLoop.Window.Draw(rect5);
            gameLoop.Window.Draw(h5);
        }
    }

    public static string GetText()
    {
        string textOfTheQuote;
        Random random = new Random();

        List<string> quotes = Quotes.quotes;


        int selector = random.Next(quotes.Count);
        
        textOfTheQuote = quotes[selector];

        quotes.Remove(textOfTheQuote);

        return textOfTheQuote;
    }

    public static void GetString()
    {
        s = GetText();
    }

    public static void TextDismiss()
    {
        if (Keyboard.IsKeyPressed(Keyboard.Key.E) && DisplayText)
        {
            SoundManager.PlayLetgo();
            
            DisplayText = false;
            FishPicture.displayFishPicture = false;
            FishPicture.renderNextFish = true;
            
            if(FisherMan.amountOfFish==10)
            {
                FisherMan.endScreen = true;
            }
        }
        if (Keyboard.IsKeyPressed(Keyboard.Key.Q) && DisplayText)
        {
            SoundManager.PlayKill();
            
            FisherMan.fishKilled++;

            DisplayText = false;
            FishPicture.displayFishPicture = false;
            FishPicture.renderNextFish = true;
            
            if(FisherMan.amountOfFish==10)
            {
                FisherMan.endScreen = true;
            }
        }
    }
}